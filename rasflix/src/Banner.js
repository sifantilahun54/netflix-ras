import React from 'react';
import './Banner.css';

function Banner() {
    return ( 
        <header className = "banner" style = 
        {{
        backgroundImage: `url('https://cdn.entertainmentdaily.com/2021/05/18163029/peaky-blinders-11-1565293761-800x529.png')`,
        backgroundSize : "cover",
        backgroundPosition: "center center",
            }}>
        <div className = "banner_contents">
        <h1 className = "banner_title">
        Peaky Blinders
        </h1>
        <div className = "banner_buttons">
        <button className = "banner_button">Play</button>
        <button className = "banner_button">Mylist </button>
        </div>
        <h1 className = "banner_discription"> A gangster family epic set in 1900s England, centering on a gang who sew razor blades in the peaks of their caps, and their fierce boss Tommy Shelby.</h1>
        </div>
        <div className = "banner_fadebutton"/>
        </header>
    )
}

export default Banner
